<?php
/**
 * NoName Studios
 * Mouse Framework
 * Mouse Cache Memcache - Interface to Memcache, provides automatic connection setup.
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2010 - 2014 NoName Studios
 * @license		All Rights Reserved
 * @package		Mouse Framework
 * @link		http://www.nonamestudios.com/
 * @version		2.0
 *
**/

class mouseCacheMemcache extends Memcached {
	/**
	 * Object Key
	 *
	 * @var		object
	 */
	public $objectKey;

	/**
	 * Constructor
	 *
	 * @access	public
	 * @param	string	[Optional] Object key used to initialize the object to mouse.  Also serves as the settings array key.
	 * @param	boolean	[Optional] Store Memcache results in local RAM for faster look up.  Default is true.
	 * @return	void
	 */
	public function __construct($objectKey = 'memcache') {
		parent::__construct();

		$this->objectKey	= $objectKey;
		$this->settings		=& mouseHole::$settings[$this->objectKey];

		//Automatic enable.
		if ($this->settings['use_memcache']) {
			$this->init();
		}

		if (isset($this->settings['use_ramcache']) && is_bool($this->settings['use_ramcache'])) {
			$this->useRAMCache = $this->settings['use_ramcache'];
		}
	}

	/**
	 * Automatically initiate memcache connection.  Utilizes the addServer() function with arguments named the same in the settings.  At least one server must be valid to return true.
	 *
	 * @access	public
	 * @return	boolean	Connection Success
	 */
	public function init() {
		$return = false;
		if (is_array($this->settings['servers']) && count($this->settings['servers']) && array_key_exists('host', $this->settings['servers'][0])) {
			foreach ($this->settings['servers'] as $server) {
				//So, I wanted to be fancy and use this line, but if the programmer puts the information out of order in the array then the arguments will be out of order.  "call_user_func_array(array($this, 'addServer'), $host);"
				$success = $this->addServer(
					$server['host'],
					(isset($server['port']) && is_numeric($server['port']) ? intval($server['port']) : 11211),
					(isset($server['weight']) && is_numeric($server['weight']) ? intval($server['weight']) : 1)
					/*(isset($server['persistent']) && is_bool($server['persistent']) ? $server['persistent'] : true),
					(isset($server['timeout']) && is_numeric($server['timeout']) ? intval($server['timeout']) : 1),
					(isset($server['retry_interval']) && is_numeric($server['retry_interval']) ? intval($server['retry_interval']) : 15),
					(isset($server['status']) && is_bool($server['status']) ? intval($server['status']) : 15),
					(isset($server['failure_callback']) && is_array($server['failure_callback']) ? $server['failure_callback'] : null)*/
				);
				if ($success == true) {
					$return = true;
				}
			}
		}

		$this->enabled = $return;

		return $return;
	}
}
