<?php
/**
 *********************************************************
 * HYDRA DEVELOPERS - PLEASE READ
 *********************************************************
 *
 * Extensions in this file are considered so critical to the operation of Hydra that they must
 * not be defined in Special:WikiAllowedExtensions as to prevent them from being
 * accidentally turned off.
 */

// This can stay in this file forever.  We do not need to move it to allowed extensions.
wfLoadExtension('RedisCache');

wfLoadExtension('DynamicSettings');
// Security for API Server Call in Dynamic Settings
$wgDSAPIServerToken = $secrets['DS_API_SERVER_TOKEN'];
$wgDSInstallDBUser = [
	'user'		=> 'wiki-spinup',
	'password'	=> $secrets['DS_INSTALL_DB_PASSWORD']
];
$wgDSInstallDBNodeConfig = [
	'cluster-xtradb' => [
		'templates' => [
			'name' => 'db{node%1}_cluster.aurora.local.curse.us',
			'dbAddress' => 'db{node%1}_cluster.aurora.local.curse.us',
			// Our master and slave end points use a dash instead of an underscore like the cluster end point.
			'dbAddressReplica' => 'db{node%1}-slave.aurora.local.curse.us',
			'dbPort' => '3306',
			'searchHost' => 'es-ec2cluster-internal-lb-1{node%2}.local.curse.us',
			'searchPort' => '9200',
			'workerNode' => 'internal-hydra-worker.local.curse.us'
		],
		'wikisPerNode' => 2000,
		'nodeCount' => 4,
		'skipNodes' => [1]
	]
];
$wgDSInstallDBUseNodeConfig = "cluster-xtradb";

if (isset($_SERVER['PHP_ENV']) && $_SERVER['PHP_ENV'] == 'development') {
	// Super Grant
	$wgDSDBUserGrants = [
		['host' => '10.20.%%.%%'],
		['host' => '10.101.%%.%%'],
	];
} else {
	// Web/Worker Nodes
	$wgDSDBUserGrants = [
		['host' => '10.125.%.%']
	];
}

$wgDSHydraHttpAuthInfo = $secrets['DS_HTTP_BASIC_AUTH'];
$wgDSGlobalPageEditorInstallAccount = [
	'user'		=> 'HydraGPEBot',
	'password'	=> $secrets['GPE_INSTALL_PASSWORD']
];

// Allows this to be overridden in LocalDevSettings.
if (!isset($wgDSGlobalPageEditorAccount)) {
	$wgDSGlobalPageEditorAccount = [
		'user'		=> 'HydraGPEBot@HydraGPEBot',
		'password'	=> $secrets['GPE_BOT_PASSWORD']
	];
}

$wgMercuryAuthDomain = $wgDefaultPortal;

$wgMercuryAuthCookieDomain = ".{$bareDomain}";
$wgMercuryAPIUrl = "https://www.{$bareDomain}/api/";
$wgGProApiConfig = [
	"endpoint"		=> "//www.{$bareDomain}/",
	"https"			=> true,
	"ssl_verify"	=> true,
	"api_key"		=> $secrets['GP_PRO_API_KEY']
];
$wgMercuryAPIKey = $secrets['MERCURY_API_KEY'];
$wgMercuryCryptKey = $secrets['MERCURY_CRYPT_KEY'];
$wgMercurySessionKey = $secrets['MERCURY_SESSION_KEY'];

$wgEchoCrossWikiNotifications = true;
// Work around for weird bug where $wgEchoCluster is not initialized in the updater.
$wgEchoCluster = false;
$wgEchoUnreadWikis = true;
$wgEchoSharedTrackingDB = 'hydra';
$wgEchoSharedTrackingCluster = 'master';
// Pretty sure we still need this as of MW 1.29.  Still a beta feature according to Echo.
$wgDefaultUserOptions['echo-cross-wiki-notifications'] = true;
$wgEchoUseJobQueue = true;

$wgCommunityManager = 'Misterwoodhouse';
$wgHLFaqUrl = 'https://help.gamepedia.com/Policies,_procedures,_and_FAQ_(One-Stop_Shop)';
$wgHLFeedbackUrl = 'https://help.gamepedia.com/Talk:Analytics_Dashboard';
$wgHLSlackUrl = 'https://help.gamepedia.com/Slack';
$wgStripGAProperties = [
	"UA-35871056-4",
	"UA-38793428-195"
];

$wgCheevosHost = 'http://cheevos.local.curse.us/v1';
$wgCheevosClientId = $secrets['CHEEVOS_CLIENT_ID'];

$wgGlobalBlockingCluster = 'master';
$wgGlobalBlockingDatabase = 'hydra';

$wgAbuseFilterCentralCluster = 'master';
$wgAbuseFilterCentralDB = 'hydra';

$wgDSMasterDBLB = 'master';
$wgDSMasterDB = 'hydra';

// Whitelist the internal ip for AWS environment
$wgLinterSubmitterWhitelist = [
	'10.125.0.0/16' => true
];

/**
 * Custom Extensions
 */
if (is_file($IP . '/settings/LocalDevExtensions.php')) {
	require $IP . '/settings/LocalDevExtensions.php';
}
