<?php
if (extension_loaded('newrelic')) {
	newrelic_ignore_transaction();
}
define('SETTINGS_ONLY', true);

$host = trim($_SERVER['HTTP_HOST']);
$check = parse_url('http://'.$host.'/',  PHP_URL_HOST);
$settingsFile = __DIR__.'/sites/'.$host.'/LocalSettings.php';
if ($host === $check && file_exists($settingsFile)) {
	ob_start();
	require($settingsFile);
	ob_end_clean();
	if (isset($wgLocalFileRepo['backend']) && strpos($wgLocalFileRepo['backend'], 's3') === 0) {
		$uri = str_replace($_SERVER['PHP_SELF'], '', $_SERVER['REQUEST_URI']);
		$uri = $uri;
		if (strpos($uri, '/media/') === 0) {
			$uri = str_replace('/media/', '', $_SERVER['REQUEST_URI']);
		}
		$parts = explode('/', trim($uri, '/'));
		if (isset($parts[0])) {
			if ($parts[0] === 'hydra') {
				$host = parse_url($wgLocalFileRepo['url'], PHP_URL_HOST);
				$wgLocalFileRepo['url'] = 'https://'.$host.'/hydra';
				array_shift($parts);
			}
			if (strpos($parts[0], '.') !== false) {
				array_shift($parts);
			}
			$uri = '/'.implode('/', $parts);
		}
		$fileUrl = $wgLocalFileRepo['url'].$uri;
		http_response_code(302);
		header('Cache-Control: public, max-age=14400');
		header('Location: '.$fileUrl);
		exit;
	}
}
http_response_code(404);